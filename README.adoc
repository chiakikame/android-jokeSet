= Readme for JokeSet

This is a small app which grab and show jokes to the user.

== Demonstrated aspects
* Keep data between screen rotation
* Grab data via HTTP protocol (using `Url.openConnection` and `AsyncTask`)
* Network status probing
* Internet-related permission

== Joke Source
API: http://www.icndb.com/
