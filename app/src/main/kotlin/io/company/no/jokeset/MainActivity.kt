package io.company.no.jokeset

import android.Manifest
import android.app.Fragment
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Resources
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.TextView
import org.apache.commons.lang3.StringEscapeUtils
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

class MainActivity : AppCompatActivity() {
    private var jokeSource: JokeSource? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // TODO: determine joke source here in the future
        jokeSource = ICNDBSource()

        val previousJoke = fragmentManager.findFragmentByTag(JokeFragment.tag) as JokeFragment?
        if (previousJoke == null) {
            getJoke()
        } else {
            updateUI(JokeFetchingResult(true, joke = JokeContent(previousJoke.jokeContent, null)))
        }
    }

    fun getAnotherJoke_onClick(v: View) {
        getJoke()
    }

    private fun getJoke() {
        if (!checkPermission()) {
            updateUI(JokeFetchingResult(false, resources.getString(R.string.mainActivity_errorMsg_permissionNotGranted)))
        }

        val connService = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connService.activeNetworkInfo?.isConnected == true) {
            val lblResult = findViewById(R.id.lblResult) as TextView
            lblResult.text = resources.getString(R.string.mainActivity_lblResult_working)
            JokeFetcher({ updateUI(it) }, jokeSource ?: throw Exception("Joke source should not be null"), resources).execute()
        } else {
            val noNetworkDetectedStr = resources.getString(R.string.mainActivity_errorMsg_noConnection)
            updateUI(JokeFetchingResult(false, errorMessage = noNetworkDetectedStr))
        }
    }

    private fun updateUI(result: JokeFetchingResult) {
        val lblResult = findViewById(R.id.lblResult) as TextView

        if (result.success) {
            lblResult.setTextColor(ContextCompat.getColor(this, R.color.normal_result))
            lblResult.text = result.joke?.content

            // Preserve joke here
            val frag = JokeFragment()
            frag.jokeContent = result.joke?.content ?: ""
            fragmentManager.beginTransaction().add(frag, JokeFragment.tag).commit()
        } else {
            val errorMessageTemplate = resources.getString(R.string.mainActivity_lblResult_error)
            lblResult.setTextColor(ContextCompat.getColor(this, R.color.error_result))
            lblResult.text = String.format(errorMessageTemplate, result.errorMessage)
        }
    }

    private data class JokeFetchingResult(val success: Boolean,
                                          val errorMessage: String? = null,
                                          val joke: JokeContent? = null)

    private class JokeFetcher(val uiUpdator: (JokeFetchingResult) -> Unit, val jokeSource: JokeSource,
                              val resources: Resources):
            AsyncTask<Unit, Unit, JokeFetchingResult>() {
        override fun doInBackground(vararg params: Unit): JokeFetchingResult {
            try {
                val connection = jokeSource.getApiUrl().openConnection() as HttpURLConnection
                connection.requestMethod = "GET"
                connection.doOutput = true
                connection.connect()

                val reader = connection.inputStream.bufferedReader()
                try {
                    val content = reader.readText()
                    return JokeFetchingResult(true, joke = jokeSource.parseJsonForContent(JSONObject(content)))
                } finally {
                    reader.close()
                }

            } catch (e: IOException) {
                return JokeFetchingResult(false, errorMessage = resources.getString(R.string.mainActivity_errorMsg_ioError))
            } catch (e: JSONException) {
                return JokeFetchingResult(false, errorMessage = resources.getString(R.string.mainActivity_errorMsg_jsonError))
            } catch (e: JokeFormatError) {
                return JokeFetchingResult(false, errorMessage = resources.getString(R.string.mainActivity_errorMsg_jokeFormatError))
            } catch (e: JokeAPIError) {
                return JokeFetchingResult(false, errorMessage = resources.getString(R.string.mainActivity_errorMsg_jokeAPIError))
            } catch (e: Exception) {
                return JokeFetchingResult(false, errorMessage = resources.getString(R.string.mainActivity_errorMsg_unexpectedError))
            }
        }

        override fun onPostExecute(result: JokeFetchingResult) {
            uiUpdator(result)
        }
    }

    private interface JokeSource {
        fun getApiUrl(): URL
        fun parseJsonForContent(json: JSONObject): JokeContent
    }

    private class ICNDBSource: JokeSource {
        override fun getApiUrl(): URL {
            return URL("http://api.icndb.com/jokes/random")
        }

        override fun parseJsonForContent(json: JSONObject): JokeContent {
            val content = if (json.getString("type") == "success") {
                val jokeObj = json.getJSONObject("value") ?: throw JokeFormatError()
                jokeObj.getString("joke") ?: throw JokeFormatError()
            } else {
                throw JokeAPIError()
            }
            return JokeContent(StringEscapeUtils.unescapeHtml4(content), null)
        }
    }

    private data class JokeContent(val content: String, val imageUrl: URL?)
    private class JokeFormatError(): Throwable()
    private class JokeAPIError(): Throwable()

    private fun checkPermission(): Boolean {
        val netPermissionGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED
        val statePermissionGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_GRANTED

        // Both are normal permissions, the system automatically grants it.
        return netPermissionGranted && statePermissionGranted
    }

    class JokeFragment(): Fragment() {
        companion object {
            val tag = "joke"
        }

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            retainInstance = true
        }

        var jokeContent: String = ""
    }
}
